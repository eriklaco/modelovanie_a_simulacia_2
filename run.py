import random, time, string

class Example:
    DIM = 500 # dlzka cesty
    AA = 100 # pozorovany usek AA, BB
    BB = 170
    road = []
    road_delta = []

    def __init__(self):
        self.road_init()
        self.show_part(0, 50)

    '''
        Inicializacia cesty
    '''
    def road_init(self):
        # znak medzera ' ' znamena prazdny element cesty bez auta
        for i in range(self.DIM):
            self.road.append(" ")
            self.road_delta.append(" ")

    '''
        Jednoducha vizualizacia hustoty premavky na ceste
    '''
    def show_road(self):
        print "\n\nsituacia na celej ceste:\n<",
        sector = 0
        for i in range(self.DIM):
            # na tomto mieste auto je
            if self.road[i] != " ":
                sector += 1
            #  za kazdy usek o dlzke (napriklad) 50 spocitame auta a ukazeme hustotu
            if i % 50 == 0 and i != 0:
                if sector >= 0 and sector < 5: print ".",
                if sector >= 5 and sector < 10: print "-",
                if sector >= 10 and sector < 15: print "+",
                if sector >= 15 and sector < 20: print "*",
                if sector >= 20 and sector <= 25: print "@",
                sector = 0
        print ">\n"


    '''
        Vizualizacia useku cesty <a,b>
    '''
    def show_part(self, a, b):
        print "\n\ncast cesty:\n<",
        for i in range(a, b):
            print "%s" % (self.road[i]),
        print ">\n"


    '''
        Prichod auta na zaciatok cesty
    '''
    def car_coming(self):
        if self.road_delta[0] != " ":
            return False
        self.road_delta[0] = random.choice(string.ascii_lowercase)


    '''
        Zistenie vzdialenosti k najblizsiemu autu vpredu
    '''
    def car_distance(self, position):
        for i in range(1, position + 1):
            if self.road[i] != " ":
                return i
        return self.DIM


    '''
        Elementarny krok vpred pre auto na danej pozicii
    '''
    def car_going(self, position):
        next = self.car_distance(position)

        if self.road[position] != " ":
            if position > self.DIM - 1:
                self.road_delta[position] = " " # opustenie cesty na konci
                return position + 1

            # od aktualnej pozicie sa posunieme nejakou rychlostou vpred:
            gt = position + (1 + random.randint(1, 3))
            print "Car: %s Position %s New: %s" % (self.road_delta[position], position, gt)
            if gt >= next:
                gt = next - 1

            self.road_delta[gt] = self.road_delta[position]
            self.road_delta[position] = " "

            return next
        return next

    '''
        Ked mame vyrobeny stav cesty v case T+delta, tak ho
        prekopirujeme do stavu v (novom) case T
    '''
    def time_step(self):
        for i in range(self.DIM):
            self.road[i] = self.road_delta[i]


    def run(self):
        print "zaciatok simulacie\n\n"
        for x in range(50):
            for i in range(10):
                self.car_coming() # prichod auta na zaciatok

                # vykonaj elementarny pohyb vsetkych aut
                position = 0
                while position < self.DIM:
                    print position
                    self.time_step() # vymen T za T+delta
                    #self.show_part(self.AA, self.BB) # ukaz detailne cast cesty
                    #self.show_road() # vizualizuj celu cestu
                    position = self.car_going(position)
                print self.road

            print "\n\nStlac Enter"
            raw_input()



a = Example()
a.run()